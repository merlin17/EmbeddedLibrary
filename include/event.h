#ifndef _EVENT_H_
#define _EVENT_H_

/**
 * @defgroup event Event Triggered Task Module
 * @ingroup task
 *
 * The Event Module is intended to be used with interrupts when a task needs
 * to run in response to the interrupt and the desire is to not call any tasks
 * from the interrupts. This module makes this easy by allowing the user to
 * register a task to run (see Event_Register()) in response to a flag being
 * set to a non zero value.
 *
 * @todo Josh Hass Add a code example (when done change this line to "@todo MM check <your names> documentation"
 *
 * @{
 */
#include <stdint.h>

#include "system.h"

#ifndef USE_MODULE_EVENT
#warning "USE_MODULE_EVENT not defined in system.h. Other modules won't be able to utilize this module."
#endif

#ifndef MAX_EVENTS
#define MAX_EVENTS 2 ///< max number of events that can be registered
#endif

/**
 * @warning JaH 9/10
 *
 * Initializes list of events (sets size to 0)
 * 
 */
void Event_Init();

/**
 * @todo Austin H. Document this function (when done change this line to "@todo MM check <your names> documentation"
 */
void Event_Tick();

/** 
 * Event Register will add an event (a function pointer) to the events array
 *
 * If the array is not full (size < MAX_EVENTS), then a new event
 * will be added at the end of the array, where the flag and the function
 * pointer are passed in as parameters.
 *
 *
 * Event_Init() must be used to initialize the events array prior to calling
 * Event_Register or Event_Unregister
 *
 * @param flag boolean flag to indicate if the event function pointer for
 * the corresponding function pointer should be called upon a sequential
 * Event_Tick() call
 *
 * @param fn_ptr function pointer to be called when Event_Tick() is called and the
 *  corresponding event in the global events array has a set flag value of 1.
 * 
 * @note the .c file for this has "fn" as the parameter name, not "fn_ptr"
 * 
 *
 * @warning LH 10/10
 */
void Event_Register(uint8_t* flag, void(*fn_ptr)(void));

/** Event_Unregister unregisters an event from the list of events that are to occur. This is done by passing a function 
 * pointer that points to the function that needs to be unregistered. Event_Unregister then loops through the event array 
 * to find the matching function pointer. When the function pointer is found the remaining items in the event array are 
 * shifted left to overwrite the function pointer.
 *
 * @param fn is a function pointer pointing to the function to unregister.
 *
 * @warning EJ 10/10
 */
void Event_Unregister(void(*fn)(void));

///@}
#endif //_EVENT_H_
