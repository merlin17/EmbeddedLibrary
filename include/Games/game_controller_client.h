/**
 * @defgroup controller_module_client Controller Module Client Service
 *
 * Provides an API that allows any arbitrary controller to communicate with a game console over a predefined protocol
 *
 *
 * Dependencies:
 * - controller_module_client.c/h
 *		- uart_packet.h
 *		- task.h
 *		- ...?
 *
 *  Created on: Feb 22, 2017
 *      Author: Christopher Green <greenc3@students.rowan.edu>
 *      		Brian Westervelt  <westerveb6@students.rowan.edu>
 *      		Tom Gracie        <graciet8@students.rowan.edu>
 *      		Josh Howlett	  <howlettj1@students.rowan.edu>
 */

#ifndef _GAME_CONTROLLER_CLIENT_H_
#define _GAME_CONTROLLER_CLIENT_H_

#include <stdint.h>

/*
*  You must define system-related parameters such as FCPU in your system.h file
*  All constants defined here are specific to use of the controller module client
*  You also MUST define two UART channels CMC_UART_UPSTREAM and CMC_UART_DOWNSTREAM
*  for this module to use with your microcontroller (Ex: MSP430 has UART0 and UART1)
* 
*/

#define CMC_UART_DEBUG 0 // Use one for debugging now...
#define CMC_DEBUG 0

#define MAX_NUM_CONTROLLERS 4

// API Functions
void GameControllerClient_Init();	// Initializes stuff
void GameControllerClient_SetAllButtons(uint16_t);
void GameControllerClient_SetAllButtonsForAddress(uint8_t, uint16_t);
void GameControllerClient_SetButton(uint8_t, uint8_t);
void GameControllerClient_SetPrimaryButtons(uint8_t); // Set d-pad buttons, start, select, A, B

#endif /* _GAME_CONTROLLER_CLIENT_H_ */
