/*
 * controller_module_client.c
 *
 *  Created on: Feb 22, 2017
 *      Author: Chris
 */

#include <stdbool.h>

#include "game_controller_client.h"
#include "game_controller.h"
#include "system.h"

#include "uart_packet.h"
#include "task.h"
#include "uart.h"
#include "buffer.h"
#include "hal_uart.h"
#include "list.h"
#include "timing.h"
#include <string.h> // for memcpy

// Move to system.h later, these are the UART interfaces specified by the embedded library that we must use
#ifndef CMC_UART_UPSTREAM
#warning "must define CMC_UART_UPSTREAM in system.h for the UART connected towards the console"
#endif

// Structures for holding packet data
typedef struct{
	uint8_t padding;			// Unused, only for data alignment
	uint8_t msgHeader; 			// Contains address and message type
	uint16_t data[4];			// Button data or additional data
} data_packet_t;

// Internal helper functions
static void upstreamCallback(uint8_t*, uint16_t);
static void downstreamCallback(uint8_t*, uint16_t);
static void sendUpstream(data_packet_t*);		// Send a packet upstream (away from console)
static void sendDownstream(data_packet_t*); 	// Send a packet downstream (towards the console)
// Internally stored data for controller module
uint8_t my_address = CADDR_NONE >> CADDR_OFFSET; 	// Initialize to the default, unassigned address TODO: Make this static again, made it non-static for debugging
static uint16_t send_interval = 500; 				// Default interval to automatically send data
static data_packet_t current_state;  				// This is the last button packet we generated or passed through, we will ignore the message header
static data_packet_t upstream_queue; 				// Queue for sending data upstream
static data_packet_t downstream_queue; 				// Queue for sending data downstream

#define debugUartPrintf(uartDebug, uartInterface, ...);  if(uartDebug) {UART_Printf(uartInterface, ##__VA_ARGS__);}

// Initialization routine for the Controller Module Client
// Configures UART_PACKET
void GameControllerClient_Init(){
	// Initialize Timing module
	Timing_Init(); // initialize the timing module first

	// Initialize task scheduler, will be used for periodically sending data
	Task_Init(); // initialize task management module next

	// Initialize two UARTs
    UART_Init(CMC_UART_UPSTREAM);		// Initialize upstream
    // Initialize UART_PACKET - Forward to next controller or host, use UART1 for easy debugging on MSP430
	UPacket_Init(CMC_UART_UPSTREAM, upstreamCallback);
#ifdef CMC_UART_DOWNSTREAM
    UART_Init(CMC_UART_DOWNSTREAM);		// Initialize downstream
    UPacket_Init(CMC_UART_DOWNSTREAM, downstreamCallback);
#endif

	// Set up our local struct to request an address
	upstream_queue.msgHeader |= (my_address << CADDR_OFFSET); 	// Set my address as the packet header
	upstream_queue.msgHeader |= CMSG_GETADDR;	  				// We want to send an address request

	// Send address request...
	sendUpstream(&upstream_queue);
	UART_Printf(CMC_UART_DEBUG, "Sending initial address request\r\n");

	Task_Schedule((task_fn_t)(sendUpstream), (void*)(&upstream_queue), 100, send_interval); // Periodically send my current button state packet upstream
}

// We received data from either the console or a controller up the stream, DO NOT append button data to this packet
static void upstreamCallback(uint8_t * data, uint16_t length){
	bool propagate_msg = true; // Let this message propagate to the next controller unless we say otherwise
	debugUartPrintf(CMC_DEBUG, CMC_UART_DEBUG, "In upstream callback!\r\n");

	// Copy packet data to a struct
	data_packet_t received_packet;
	memcpy(&received_packet, data, sizeof(data_packet_t));

	// If this message is for me, process it.  Otherwise ignore and send down the stream
	uint8_t destination_address = ((received_packet.msgHeader) & CADDR_MASK) >> CADDR_OFFSET; // Parse address
	debugUartPrintf(CMC_DEBUG, CMC_UART_DEBUG, "I received a message for address 0x%x\r\n", destination_address);

	// This must be a control message, figure out which one
	uint8_t message_type = (received_packet.msgHeader) & CMSG_MASK;
	debugUartPrintf(CMC_DEBUG, CMC_UART_DEBUG, "I received a message of raw type 0x%x\r\n", message_type);

	// Process message IFF the message is destined for me, or if the destination is the broadcase address, or if my address is not set and message is a setaddress
	if((destination_address == my_address) || (destination_address == (CADDR_BROADCAST >> CADDR_OFFSET)) || (my_address == (CADDR_NONE >> CADDR_OFFSET) && message_type == CMSG_SETADDR)){
		switch(message_type){
		case CMSG_GETADDR: {
				debugUartPrintf(CMC_DEBUG, CMC_UART_DEBUG, "Got a getaddress message\r\n");
				// Ignore this type of message, it shouldn't ever come from upstream
				propagate_msg = false;
				break;
		}
		case CMSG_SETADDR: {
			debugUartPrintf(CMC_DEBUG, CMC_UART_DEBUG, "Got a setaddress message\r\n");
			// Parse address, will be contained in the highest 3 bits of the second byte of the header data field
			my_address = (received_packet.data[0] & 0x07);

			// now that we have our address we need to change the current state message to be a
			// button message
			current_state.msgHeader |= (my_address << CADDR_OFFSET); // Set an unassigned address
			current_state.msgHeader |= CMSG_BUTTON;	  // We want to send an address request
			upstream_queue.msgHeader = current_state.msgHeader;
			propagate_msg = false;
			break;
		}
		case CMSG_BUTTON: {
			debugUartPrintf(CMC_DEBUG, CMC_UART_DEBUG, "Got a got a button message (that we shouldn't get)\r\n");
			// Ignore this type of message and don't let it propagate, it shouldn't ever come from upstream
			propagate_msg = false;
			break;
		}
		case CMSG_SETPERIOD: {
			debugUartPrintf(CMC_DEBUG, CMC_UART_DEBUG, "Got a setperiod message\r\n");
			// Change our default sending period, parse as a uint16_t located in the first 16 data bits
			send_interval = received_packet.data[0];

			// Reschedule Tasks
			Task_Remove((task_fn_t)(sendUpstream), 0);
			Task_Remove((task_fn_t)(sendDownstream), 0);

			// Delay initial sending by send_interval just for good measure
			Task_Schedule((task_fn_t)(sendUpstream), (void*)(&upstream_queue), send_interval, send_interval); // Double check this
			Task_Schedule((task_fn_t)(sendDownstream), (void*)(&downstream_queue), send_interval, send_interval); // Double check this too

			break;
		}
			default:
				debugUartPrintf(CMC_DEBUG, CMC_UART_DEBUG, "Got an unknown message\r\n");
				break;
			}
	}
	// If the packet isn't for me, or is to the broadcast address, send downstream immediately.
	// We want quick downstream propagation
	if((propagate_msg) || (destination_address == CADDR_BROADCAST)){
		sendDownstream(&received_packet);
	}
}
	
// We received data from a controller downstream (from the perspective of the console), append my button data and send upstream
static void downstreamCallback(uint8_t * data, uint16_t length){
	bool propagate_msg = true;

	// Copy packet data to a struct
	data_packet_t *received_packet = (data_packet_t*)(data);

	// Parse message type
	uint8_t message_type = received_packet->msgHeader & CMSG_MASK;

	// TODO: add other message types
	switch(message_type){
		case CMSG_GETADDR: {
			// If a controller downstream asks for an address, assign it the address that is one greater than mine and do not let message propagate
			// If I have an unassigned address, or my address is MAX_NUM_CONTROLLERS-1, than ignore the message
			if(((my_address < MAX_NUM_CONTROLLERS-1) && my_address) != (CADDR_NONE >> CADDR_OFFSET)){
				//data_packet_t assign_address_msg;
				downstream_queue.msgHeader = CMSG_SETADDR; 	// Form header of packet to send
				downstream_queue.msgHeader |= CADDR_NONE;		// Destination address is the unassigned address
				downstream_queue.data[0] = my_address+1;		// Assign the downstream controller an address one greater than mine
				sendDownstream(&downstream_queue);			// Send the address assign message immediately back to the requester
			}
			propagate_msg = false;
			break;
		}
		case CMSG_SETADDR: {
			// We should never receive one of these messages from downstream
			propagate_msg = false;
			break;
		}
		case CMSG_BUTTON: {
			// Fill in the data field corresponding to my address, pull from the current_state struct because that will be accurate
			if(my_address < MAX_NUM_CONTROLLERS){
				received_packet->data[my_address] = current_state.data[my_address];
			}

			// Check to see if button data for other has changed or not, if it has then send the message immediately
			// Otherwise just put the packet in the upstream queue to send at the periodic interval
			bool identical = true;
			int i = 0;
			for(i = 0; i < MAX_NUM_CONTROLLERS; i++){
				if((current_state.data[i]^received_packet->data[i]) > 0){ // Look for differences in button data
					identical = false;
				}
			}

			if(!identical){ // Send upstream immediately
				sendUpstream(received_packet);
				current_state = *received_packet; // Update the current state struct with the current button data
			}
			break;
		}
		case CMSG_SETPERIOD: {
			// A downstream controller should never try to set period
			propagate_msg = false;
			break;
		}
		default:
		break;
	}
	if(propagate_msg){
		upstream_queue = *received_packet; // Set up this message to propagate on the next send occurance
	}
}

static void sendUpstream(data_packet_t *to_send){
	UPacket_Send(CMC_UART_UPSTREAM, (uint8_t*)(to_send), sizeof(data_packet_t));
	debugUartPrintf(CMC_DEBUG, CMC_UART_DEBUG, "Sending upstream\r\n");
}

static void sendDownstream(data_packet_t *to_send){
#ifdef CMC_UART_DOWNSTREAM
	UPacket_Send(CMC_UART_DOWNSTREAM, (uint8_t*)(to_send), sizeof(data_packet_t));
	debugUartPrintf(CMC_DEBUG, CMC_UART_DOWNSTREAM, "Sending downstream\r\n");
#endif
}

void GameControllerClient_SetAllButtons(uint16_t buttons){
	GameControllerClient_SetAllButtonsForAddress(my_address, buttons); // call with local variable with address
}

// This function accepts addresses from 0-MAX_NUM_CONTROLLERS - 1, do not pass raw data parsed from the address field without bit shifting
void GameControllerClient_SetAllButtonsForAddress(uint8_t address, uint16_t buttons){
	if(address < MAX_NUM_CONTROLLERS) {
		if((current_state.data[address]^buttons) != 0) { // Check to see if buttons have changed and the address is valid
			debugUartPrintf(CMC_DEBUG, CMC_UART_DEBUG, "Detected a button change!\r\n");
			upstream_queue.msgHeader = CMSG_BUTTON; // Address doesn't need to be defined, these messages are implied to be destined for the host
			upstream_queue.data[address] = buttons;

			current_state.msgHeader = upstream_queue.msgHeader;
			current_state.data[address] = upstream_queue.data[address];

			sendUpstream(&upstream_queue); // We may want to schedule a task to send this data upstream instead
		}
	}
}

void GameControllerClient_SetButton(uint8_t button_index, uint8_t value){
	if(value == 0x01){
		current_state.data[my_address] |= (1 << button_index);
	}
	else if (value == 0x00) {
		current_state.data[my_address] &= ~(1 << button_index);
	}
	// Otherwise, do nothing. They are using it incorrectly.

	sendUpstream(&current_state);

}
void GameControllerClient_SetPrimaryButtons(uint8_t primary_buttons){
	GameControllerClient_SetAllButtonsForAddress(my_address, (uint16_t)primary_buttons);
}
