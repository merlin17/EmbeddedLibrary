#include "system.h"
#include "hal_general.h"
#include "int_def.h"
#include "hal_keyfob_receiver.h"
#include "keyfob_receiver.h"

// hard coded to use RPF5 on IC4 and use TMR2/3 as timer source

void hal_Keyfob_Init(void) {
    // set IC4 to RPF5
    IC4R = 2;
    IC4CON = 0x00008181; // TMR2 is source, capture every edge, 32-bit mode
    T2CON = 0x00008068; // 32-bit timer, 1:64 prescale
    PR2 = 0xFFFFFFFF;
    TMR2 = 0;
    IPC4bits.IC4IP = 3;
    IFS0bits.IC4IF = 0;
    IEC0bits.IC4IE = 1;
}

// hal interrupt
void __attribute__((vector(_INPUT_CAPTURE_4_VECTOR), interrupt(), nomips16)) _IC4_ISR(void) {
    uint16_t time;
    static uint32_t prev_count = 0;
    uint32_t count;

    while (IC4CONbits.ICBNE) {
        // get time
        count = IC4BUF;
        
        if(count > prev_count){
            time = (count - prev_count);
        }
        else{
            time = ((0xFFFFFFFF - prev_count) + count);
        }
        prev_count = count;
        Keyfob_ProcessPulse(time);
    }
    IFS0bits.IC4IF = 0;
}
