#include "system.h"
#include "nrf24network.h"
#include "jj_chat.h"
#include "big_int.h"


static uint8_t sys_id;
static uint8_t address;
static uint8_t data[10];
static uint8_t length;
static uint8_t thing = 16;


static big_int_t key;

 



enum encrypt_msg{
    REQUEST_RSA = 0,
    SEND_RSA,
    FIRST_MSG,
    MID_MSG,
    LAST_MSG
};

static void TEA_Callback(int argc, char * argv[]);
static void TEA_Receiver(char c);
static void TEA_Handler(uint8_t *data, uint8_t len, uint8_t from);

void JJ_Chat_Init(void) {
    nrf24_RegisterMsgHandler(CHAT_TEA_MSG, TEA_Handler);
    version_t v;
    v.word = 0x00000000;
    sys_id = Subsystem_Init("TEA",v,TEA_Callback);
    big_init(&key,thing);
    key.bytes[0] = 0;
    key.bytes[1] = 1;
    key.bytes[2] = 2;
    key.bytes[3] = 3;
    key.bytes[4] = 4;
    key.bytes[5] = 5;
    key.bytes[6] = 6;
    key.bytes[7] = 7;
    key.bytes[8] = 8;
    key.bytes[9] = 9;
    key.bytes[10] = 10;
    key.bytes[11] = 11;
    key.bytes[12] = 12;
    key.bytes[13] = 13;
    key.bytes[14] = 14;
    key.bytes[15] = 15;
}

void TEA_Callback(int argc, char * argv[]) {
    if(argc == 0) LogMsg(sys_id,"too few args");
    
    address = AddressFromName(argv[0]);
    if(address==1) LogMsg(sys_id,"invalid name");
    else {
        length = 1;
        data[0] = FIRST_MSG;
        UART_Printf(SUBSYS_UART, "Msg: ");
        UART_RegisterReceiver(SUBSYS_UART, TEA_Receiver);


    }
}

void TEA_Receiver(char c) {
    uint8_t i;

    // if carriage return then send message and unregister receiver
    static uint8_t first = 1;
    
    if (first == 1)
    {
     first = 0;
     return;
    }

    if(c == '\r') {
        data[0] = LAST_MSG;
        for(i=length+1;i<9;i++) data[i] = ' ';
        TEA_encrypt(&data[1],&key);
        data[9] = 0;
        nrf24_SendMsg(address, CHAT_TEA_MSG, &data[0], 10);
        UART_UnregisterReceiver(SUBSYS_UART, TEA_Receiver);
        first =1;
    }
    else data[length++] = c;
    
    // if message is full then send message
    if(length >= 9) {
        TEA_encrypt(&data[1],&key);
        data[9] = 0;
        nrf24_SendMsg(address, CHAT_TEA_MSG, &data[0], 10);
        data[0] = MID_MSG;
        length = 1;
    }
}

void TEA_Handler(uint8_t *data, uint8_t len, uint8_t from) {
    enum encrypt_msg id;
    id = *data++;
    switch(id) {
        case REQUEST_RSA:
        
            break;
        case SEND_RSA:
            
            break;
        case FIRST_MSG:
            TEA_decrypt(&data[0],&key);
            UART_Printf(SUBSYS_UART, "%s: %s", NameFromAddress(from), data);
            break;
        case MID_MSG:
            TEA_decrypt(&data[0],&key);
            UART_Printf(SUBSYS_UART, "%s", data);
        case LAST_MSG:
            TEA_decrypt(&data[0],&key);
            UART_Printf(SUBSYS_UART, "%s\r\n", data);
            break;
    }
}
