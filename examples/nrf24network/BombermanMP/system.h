/* 
 * File:   system.h
 * Author: George
 *
 * Created on March 8, 2015, 9:17 PM
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#define FCPU 40000000
#define PERIPHERAL_CLOCK FCPU

#define USE_SPI1
#define SPI_MAX_SIZE 33

#define USE_UART2
#define SUBSYS_UART 2
#define PLAYER1_UART SUBSYS_UART

#define THIS_NODE LECAKES
#define BOMBERMAN_SERVER

#include "library.h"
#include "task.h"
#include "timing.h"
#include "spi.h"
#include "uart.h"
#include "buffer.h"
#include "buffer_printf.h"
#include "list.h"
#include "nrf24.h"
#include "nrf24network.h"
#include "subsys.h"
#include "charReceiverList.h"
#include "game.h"
#include "bomberman.h"

#endif	/* SYSTEM_H */

